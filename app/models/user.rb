class User < ActiveRecord::Base
  after_initialize :set_admin, :if => :new_record?

  def set_admin
    self.admin ||= false
  end

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
