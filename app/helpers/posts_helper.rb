class CodeRayify < Redcarpet::Render::HTML
  def block_code(code, language)
    CodeRay.scan(code, language).div(line_numbers: :table, css: :class)
  end
end

module PostsHelper
  def markdown text
    render_options = {
      filter_html:     true,
      hard_wrap:       true,
      link_attributes: { rel: 'nofollow' },
      prettify:        true
    }
    renderer = CodeRayify.new(render_options)
    extensions = {
      autolink:           true,
      fenced_code_blocks: true,
      lax_spacing:        true,
      no_intra_emphasis:  true,
      strikethrough:      true,
      superscript:        true
    }
    find_and_preserve Redcarpet::Markdown.new(renderer, extensions).render(text).html_safe.gsub("%%%summary%%%","")
  end

  def summarize text
    find_and_preserve text.split("%%%summary%%%").first
  end
end
