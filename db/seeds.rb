user = User.find_or_create_by!(email: Rails.application.secrets.admin_email) do |user|
  user.password = Rails.application.secrets.admin_password
  user.password_confirmation = Rails.application.secrets.admin_password
  user.admin = true
  user.save
end

puts "Created user #{user.email}"

if ENV['RAILS_ENV'] == 'development' or ENV['RAILS_ENV'] == 'test'
  testuser = User.find_or_create_by!(email: 'test@example.com') do |user|
    user.password = 'changeme'
    user.password_confirmation = 'changeme'
    user.save
  end

  puts "Created user #{testuser.email}"

  [1,2,3,4].each do |i|
    title = Lorem::Base.new('words', 7).output
    body = Lorem::Base.new('paragraphs', i).output
    Post.new(title: title, body: body).save
    puts title
  end

  puts "Created 5 random lorem ipsum posts"
end
