# Creates an application template with authentication (Devise) and CSS (Twitter Bootstrap)

## Use
Use as a regular Rails template

```
rails new blog -m https://bitbucket.org/nowayride/blog-template/raw/master/application_template.rb
```

## Login
Add login links to Bootstrap nav menu

```ruby
.navbar-collapse.collapse.navbar-responsive-collapse
  %ul.nav.navbar-nav
    %li= link_to "Link 1", "/path1"
    %li= link_to "Link 2", "/path2"
    %li= link_to "Link 3", "/path3"
    %li= link_to "Link 3", "/path3"
  = render 'login_links'
```

## Protect application for Devise

```ruby
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
    before_action :configure_permitted_parameters, if: :devise_controller?

    protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username, :email, :password, :password_confirmation, :remember_me) }
      devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :username, :email, :password, :remember_me) }
      devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:username, :email, :password, :password_confirmation, :current_password) }
    end
end
```

## Restrict an element to admin

The admin field is added to users. To manage administrative access, modifying the "admin" field needs to be added (users by default will not have admin). See https://github.com/plataformatec/devise/wiki/How-To:-Add-an-Admin-role for more information (option 2).
