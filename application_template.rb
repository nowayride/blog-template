def blue message
  say "\033[1m\033[36m#{message}\033[0m"
end

def resource path
  get "https://bitbucket.org/nowayride/blog-template/raw/master/#{path}", path, force: true
end

# Gems
gem 'haml-rails'
gem 'devise'
gem 'carrierwave'

gem 'redcarpet'
gem 'coderay'

gem 'twitter-bootstrap-rails'

gem 'less-rails'
gem 'therubyracer', platforms: :ruby

gem_group :development, :test do
  gem 'lorem'
end


after_bundle do
  # Twitter Bootstrap
  generate 'bootstrap:install', :less

  # Setup devise
  generate 'devise:install'
  generate :devise, :user
  generate :migration, :AddAdminToUsers, 'admin:boolean'

  # Setup post scaffold
  generate :scaffold, :post, 'title:string', 'body:text'

  route "root 'posts#index'"

  # Migrate the database
  rake 'db:migrate'

  # Setup the general Bootstrap theme
  run 'find . -name *.erb -delete'
  generate 'bootstrap:layout', 'application', '-f'
  generate 'bootstrap:themed', :Posts, '-f'

  # Create layout helpers
  resource 'db/seeds.rb'
  resource 'app/assets/stylesheets/bootstrap_and_overrides.css.less'
  resource 'app/controllers/posts_controller.rb'
  resource 'app/helpers/posts_helper.rb'
  resource 'app/models/user.rb'
  resource 'app/views/application/_login_links.html.haml'
  resource 'app/views/layouts/application.html.haml'
  resource 'app/views/posts/_form.html.haml'
  resource 'app/views/posts/index.html.haml'
  resource 'app/views/posts/show.html.haml'

  # todo take email from ask parameter
  ['development', 'test'].each do |env|
    inject_into_file 'config/secrets.yml', "  admin_email: admin@example.com\n  admin_password: changeme\n", after: "#{env}:\n"
  end

  inject_into_file 'config/secrets.yml', "  admin_email: <%= ENV['ADMIN_EMAIL'] %>\n  admin_password: <%= ENV['ADMIN_PASSWORD'] %>\n", after: "production:\n"

  # Seed the database
  rake 'db:seed'

  # Configure git
  git :init
  git add: "."
  git commit: %Q{ -m 'Initial commit' }

  blue 'Application ready. See https://bitbucket.org/nowayride/blog-template for more information.'
  blue 'Users admin@example.com and test@example.com available in development. Log in with "changeme"'

end
